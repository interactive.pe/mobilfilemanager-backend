<?php
namespace GQL;
use Phalcon\Config;
use Phalcon\DiInterface;
use PhalconApi\Api;

interface ContenedorInterface {

	public function lanzar(Api $api, DiInterface $di, Config $config);
}
