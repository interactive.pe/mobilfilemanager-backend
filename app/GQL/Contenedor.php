<?php
Namespace GQL;

class Contenedor
{
	protected $_ejecutables;
	public function __construct(...$ejecutables)
	{
		$this->_ejecutables = $ejecutables;
	}
	public function lanzar(...$args)
	{
		foreach ($this->_ejecutables as $ejecutable) {
			call_user_func_array([$ejecutable, 'lanzar'], $args);
		}
	}
}
