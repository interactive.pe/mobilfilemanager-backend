<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

use Phalcon\Mvc\Micro\Collection as MicroCollection;

$api = new MicroCollection();
// Set the main handler. ie. a controller instance
$api->setHandler('ApiController', true);

$api->setPrefix('/api');

$api->map('/', 'index');
$api->post('/autenticar', 'autenticar');
$api->post('/subir', 'subir');

$api->get('/categorias', 'categorias');
//$api->get( '/carpetas/{id}', 'carpetas' );
//$api->get( '/archivos/{carper}', 'archivos' );

$api->map('/usuario/{id}', 'usuario');
$api->map('/usuario/{id}/carpetas', 'usuario_carpetas');
$api->map('/usuario/{id}/favoritos', 'usuario_favoritos');
$api->map('/usuario/{id}/perfil', 'usuario_perfil');

$api->map('/carpeta/{id}', 'carpeta');
$api->map('/carpeta/{id}/archivos', 'carpeta_archivos');
$api->map('/carpeta/{id:[0-9]*}/enlace', 'carpeta_enlace');

//$api->map( '/archivos', 'usuario_archivo' );

$api->map('/categoria/{id}', 'categoria');
$api->map('/categoria/{id}/archivos', 'categoria_archivos');

$api->map('/archivo/{id}', 'archivo');
$api->map('/archivo/{id}/descargar', 'archivo_descargar');
$api->map('/archivo/{id}/etiquetas', 'archivo_etiquetas');
$api->map('/archivo/{id}/previo', 'archivo_previo');
$api->map('/archivo/{id}/miniatura', 'archivo_miniatura');
$api->map('/archivo/{id:[0-9]*}/enlace', 'archivo_enlace');

$api->map('/etiquetas', 'etiquetas');
$api->map('/etiqueta/{id}', 'etiqueta');
$api->map('/etiqueta/{id}/archivos', 'etiqueta_archivos');
$api->map('/etiquetas/todas', 'todasEtiquetas');

//$api->get( "/buscar", "buscar" );
$api->get("/buscar/{categoria:[0-9]*}", "buscar");

$api->post("/plupload", "plupload");

$app->mount($api);
