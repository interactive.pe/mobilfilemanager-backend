<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

use Phalcon\Mvc\Micro\Collection as MicroCollection;

$compartidos = new MicroCollection();
// Set the main handler. ie. a controller instance
$compartidos->setHandler('CompartidosController', true);

$compartidos->setPrefix('/compartidos');

$compartidos->map('/', 'index');
//$compartidos->map( '/carpeta/{id:[0-9]+}', 'carpeta' );
$compartidos->map('/carpeta/{token:.*}', 'carpeta');
$compartidos->map('/archivo/{token:.*}', 'archivo');
$compartidos->map('/archivo/{token:.*}/descargar', 'archivo_descargar');

$app->mount($compartidos);
