<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */


use Phalcon\Mvc\Micro\Collection as MicroCollection;

$clientes = new MicroCollection();

// Set the main handler. ie. a controller instance
$clientes->setHandler('IndexController', true);

$clientes->get('/', 'index');

$app->mount($clientes);
