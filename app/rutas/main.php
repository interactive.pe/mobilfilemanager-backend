<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */


include APP . "/rutas/clientes.php";
include APP . "/rutas/api.php";
include APP . "/rutas/compartidos.php";


$app->options('/{catch:(.*)}', function() use ($app) {
	$app->response->setStatusCode(200, "OK")->send();
});

$app->notFound(function () use($app) {
	$app->response->setStatusCode(404, "Not Found")->sendHeaders();
	echo $app['view']->render('404');
});
