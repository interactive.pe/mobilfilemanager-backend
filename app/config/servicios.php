<?php

use \Phalcon\Crypt;
use Phalcon\Mvc\View\Engine\Volt;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Simple as View;

/**
 * Shared configuration service
 */
$di->setShared('config', function () {
	return new \Phalcon\Config\Adapter\Ini(APP . "/../config.ini");
});

$di->setShared(
	'voltService',
	function ($view, $di) {
		$volt = new Volt($view, $di);

		$volt->setOptions(
			[
				'compiledPath'      => APP . '/compilados/',
				'compiledExtension' => '.compiled',
			]
		);
		$compiler = $volt->getCompiler();
		//$compiler->addFunction('urlBase', function($resolvedArgs, $exprArgs){ return urlBase(); });

		return $volt;
	}
);

/**
 * Sets the view component
 */
$di->setShared('view', function () {
	$config = $this->getConfig();

	$view = new View();
	$view->setViewsDir($config->application->viewsDir);
	$view->registerEngines(
		[
			'.volt' => 'voltService',
			'.phtml' => 'Phalcon\Mvc\View\Engine\Php',
		]
	);
	return $view;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
	$config = $this->getConfig();

	$url = new UrlResolver();
	$url->setBaseUri($config->application->baseUri);
	return $url;
});

$di->setShared('direcciones', function () {
	return new Urls();
});

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->setShared('db', function () {
	$config = $this->getConfig();

	$class = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
	$params = [
		'host'	 => $config->database->host,
		'username' => $config->database->username,
		'password' => $config->database->password,
		'dbname'   => $config->database->dbname,
		'charset'  => $config->database->charset
	];

	if ($config->database->adapter == 'Postgresql') {
		unset($params['charset']);
	}

	$connection = new $class($params);

	return $connection;
});


$di->setShared(
	'criptex',
	function () {
		$config = $this->getConfig();
		$crypt = new Crypt();

		$crypt->setCipher('aes-128-cbc');
		$crypt->setKey(
			md5($config->authentication->secret)
		);

		return $crypt;
	}
);


// $crypt = new Crypt();
// var_dump($crypt->getAvailableCiphers());
// exit;
