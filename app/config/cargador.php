<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerNamespaces(
	[
		'Phalcon' => VENDOR . '/phalcon/incubator/Library/Phalcon/',
		'Docbox\Model' => $config->application->modelsDir
	]
);

$loader->registerDirs(
	[
		$config->application->controllersDir,
		$config->application->modelsDir,
		$config->application->utilDir,
	]
);

$loader->register();
