<?php

class IndexController extends ControllerBase
{

	public function index()
	{
		return $this->view->render("index");
	}

}

