<?php

use Phalcon\Image;
use \Phalcon\Crypt;
use Phalcon\Mvc\Url;
use Docbox\Model\Subidas;
use Docbox\Model\Archivos;
use Docbox\Model\Carpetas;
use Docbox\Model\Sesiones;
use Docbox\Model\Usuarios;
use Docbox\Model\Etiquetas;

use Docbox\Model\Favoritos;
use Docbox\Model\Categorias;

use \Phalcon\Security\Random;
use Phalcon\Image\Adapter\Gd;
use Phalcon\Image\Adapter\Imagick;


class ApiController extends ControllerBase
{

	public function index()
	{
		//$c = Categorias::find();
		//return $this->renderizaTexto( $c );
		// var_dump( $this->request->getMethod() );
		// var_dump( $this->request->getRawBody() );
		// var_dump( file_get_contents('php://input'));
		// $json = $this->request->getJsonRawBody();
		// var_dump( $json );
		// var_dump( $_SERVER );

		// $url = new Url();
		// var_dump( $url->getBaseUri());
		// var_dump( $this->request->getHttpHost() );
		// var_dump( $this->request->getServer("SERVER_PROTOCOL") );


		//$this->direcciones->urlBase();
		//var_dump( $this->direcciones->urlBase() );
		//var_dump( WEB );
	}

	public function autenticar()
	{
		$datos = [];
		$this->response->setContentType("application/json", "UTF-8");

		$json = $this->request->getJsonRawBody();
		$usuario = Usuarios::findFirst("correo='{$json->usuario}'");

		if ($usuario->clave == $json->clave) {
			$random = new Random();
			$sesion = new Sesiones();
			$sesion->usuario = $usuario->id;
			$sesion->agente = $this->request->getUserAgent();
			$sesion->ip = $this->request->getClientAddress();
			$sesion->token = $random->base64Safe(127);

			if ($sesion->save() === false) {
				$this->response->setStatusCode(500, utf8_decode("Imposible crear sesión"));
				//$r["resultado"] = "Imposible crear sesión";
				$datos["mensajes"]  = $sesion->getMessages();

				//$this->response->setStatusCode( 401, "No autorizado" );

			} else {
				//$r["resultado"] = "Sesión creada";
				//$this->response->setStatusCode( 500, "Imposible crear sesión" );
				$this->response->setStatusCode(200, utf8_decode("Sesión creada"));
				$datos["sesion"] = [
					"token" => $sesion->token,
					"usuario" => $usuario->toArray([
						"id",
						"nombre",
						"correo",
						"rol"
					]),
					"disco" => $usuario->disco
				];
			}
		} else {
			$datos["ok"] = false;
			$datos["resultado"] = "Credenciales Incorrectas";

			$this->response->setStatusCode(401, "Credenciales Incorrectas");
		}

		$this->response->setJsonContent($datos);
		//$this->response->send();

		return $this->response;
	}


	public function subir()
	{
		$r = [];
		$this->response->setContentType("application/json", "UTF-8");

		$json = $this->request->getJsonRawBody();
		$fichero = sha1($json->nombre);
		$temp = PUBLICA . "/temp/" . sha1($json->usuario) . $fichero;
		//var_dump(PUBLICA . "/disco/" . $carpeta);
		//echo shell_exec( "whoami" );

		//$crc = crc32($json->contenido);
		$contenido = base64_decode($json->contenido);
		$valido = strlen($contenido) == $json->largo;//$crc == $json->crc;

		if ($valido ) {

			$p = fopen($temp, "a");
			fwrite($p, $contenido);
			fclose($p);

			$su = new Subidas([
				"id" => $json->id,
				"archivo" => $temp,
				"avance" => "$json->parte - $json->total",
				"usuario" => $json->usuario
			]);
			$su->save();

			$this->response->setStatusCode(200, utf8_decode("archivo guardado"));
			$this->response->setJsonContent([
				"ok" => true,
				"subida" => $json->parte,
				//"crc" => $crc,
				//"src" => $json,
				"largo" => strlen($contenido)
			]);
		}
		else {
			$this->response->setStatusCode(515, utf8_decode("error de transmisión"));
			$this->response->setJsonContent([
				"ok" => false,
				//"crc" => $crc,
				"subida" => $json->parte,
				//"src" => $json,
				"largo" => strlen($json->contenido)
			]);
		}
		//$this->response->send();
		return $this->response;
	}

	public function categorias()
	{
		$cat = Categorias::findFirst();
		$this->response->setJsonContent(Categorias::arbol());
		//$this->response->setJsonContent( $cat->subcategorias );

		return $this->response;
	}

	public function categoria($id)
	{
		$d = Categorias::findFirst($id);
		$this->response->setJsonContent($d);
		return $this->response;
	}

	public function carpetas($id)
	{
		$this->response->setJsonContent(Carpetas::arbol($id));

		return $this->response;
	}

	public function carpeta($id)
	{
		if (empty($id)) {
			return $this->response;
		}

		$datos = [];
		switch ($this->request->getMethod()) {
			case "DELETE":
				$carpeta = Carpetas::findFirst($id);
				if ($carpeta) {
					$contenedora = $carpeta->contenedora;
					if ($carpeta->delete() === false) {
						$datos["ok"] = false;
						$messages = $robot->getMessages();

						$datos["mensajes"] = [];
						foreach ($archivo->getMessages() as $message) {
							$datos["mensajes"][] = $message->getMessage();
						}
					} else {
						$datos["ok"] = true;
						$datos["contenedora"] = $contenedora;
					}
				} else {
					$datos["ok"] = true;
					$datos["mensajes"] = ["la carpeta no existe"];
				}

				break;
			case 'GET':
				$carpeta = Carpetas::findFirst($id);
				//$datos = $carpeta->toArray();
				//$datos[ "ruta" ] =  $carpeta->ruta;
				$datos =  $carpeta->toArray();
				$datos["camino"] =  $carpeta->camino;
				//$datos[ "raiz" ] =  $carpeta->raiz;
				//$datos[ "ud" ] = $carpeta->usuario->disco;

				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);

		return $this->response;
	}

	public function etiqueta($id)
	{
		if (empty($id)) {
			return $this->response;
		}

		$datos = [];
		switch ($this->request->getMethod()) {
			case 'GET':
				$etiqueta = Etiquetas::findFirst($id);
				//$datos = $carpeta->toArray();
				//$datos[ "ruta" ] =  $carpeta->ruta;
				$datos =  $etiqueta->toArray();
				//$datos[ "raiz" ] =  $carpeta->raiz;
				//$datos[ "ud" ] = $carpeta->usuario->disco;

				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);

		return $this->response;
	}

	public function etiqueta_archivos($id)
	{
		if (empty($id)) {
			return $this->response;
		}
		$tiposGráficos = [
			"png",
			"jpg",
			"gif"
		];

		$datos = [];
		$etiqueta = Etiquetas::findFirst($id);
		switch ($this->request->getMethod()) {
			case 'GET':
				//$datos =  $etiqueta->archivos;
				$archivos = $etiqueta->archivos;
				foreach ($archivos as $archivo) {
					$actual = $archivo->toArray();
					$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
					if (file_exists($miniatura)) {
						//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

						//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
						$img = new Imagick($miniatura);
						$tamaño = 128;
						if (
							$img->getHeight() > $tamaño ||
							$img->getWidth() > $tamaño
						) {
							$img->resize(
								$tamaño,
								$tamaño,
								Image::AUTO
							);
						}
						$imagen = base64_encode($img->render());
						$formato = $img->getMime();
						$actual["miniatura"] =  "data:$formato;base64,$imagen";
					} else if (file_exists($archivo->getRuta()) && in_array($archivo->tipo, $tiposGráficos)) {
						//$actual[ "miniatura" ] = $miniatura;
						$img = new Imagick($archivo->getRuta());
						$tamaño = 128;
						if (
							$img->getHeight() > $tamaño ||
							$img->getWidth() > $tamaño
						) {
							$img->resize(
								$tamaño,
								$tamaño,
								Image::AUTO
							);
						}
						$imagen = base64_encode($img->render());
						$formato = $img->getMime();
						$actual["miniatura"] =  "data:$formato;base64,$imagen";
					}
					$datos[] = $actual;
				}
				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);

		return $this->response;
	}

	public function archivos()
	{

		//$this->response->setJsonContent(Carpetas::arbol());

		print_r($this->dispatcher->getParam('id'));

		//return $this->response;
	}

	public function archivo($id)
	{
		//print_r( $id );
		$datos = array();
		switch ($this->request->getMethod()) {
			case 'DELETE':
				$archivo = Archivos::findFirst($id);
				if ($archivo->delete() === false) {
					$datos["ok"] = false;
					$messages = $robot->getMessages();

					$datos["mensajes"] = [];
					foreach ($archivo->getMessages() as $message) {
						$datos["mensajes"][] = $message->getMessage();
					}
				} else {
					$datos["ok"] =  true;
				}
				break;
			case 'POST':
				$json = $this->request->getJsonRawBody();
				$archivo = Archivos::findFirst($id);
				$archivo->categoria = $json->categoria;
				$archivo->nombre = $json->nombre;
				$archivo->descripcion = $json->descripcion;
				//$archivo->descripcion = $json->descripcion;
				$ids = empty($json->etiquetas->ids) ? [] : $json->etiquetas->ids;
				$nuevas = empty($json->etiquetas->nuevas) ? [] : $json->etiquetas->nuevas;
				$archivo->asignarEtiquetas($ids, $nuevas);

				if ($json->miniatura) {
					$re = '/^data:(?<formato>[a-z\/]+);base64,(?<datos>[A-Z0-9a-z+\/]{80,}[=]{0,3})$/su';
					if (preg_match($re, $json->miniatura, $captura)) {
						$fichero = $archivo->archivo;

						list(, $extension) = explode("/", $captura["formato"]);

						$min = DISCO . "/miniaturas/$fichero.$extension";
						file_put_contents($min, base64_decode($captura["datos"]));
					}
				}
				if ($archivo->save()) {
					$archivo->refresh();
					$datos["ok"] =  true;
					$datos["archivo"] = $archivo->toArray();
				} else {
					$datos["ok"] = false;
					$messages = $robot->getMessages();

					$datos["mensajes"] = [];
					foreach ($archivo->getMessages() as $message) {
						$datos["mensajes"][] = $message->getMessage();
					}
				}
				break;
			case 'GET':
				$archivo = Archivos::findFirst($id);
				$datos = $archivo->toArray();
				//$datos["categoria"] = $d->ubicacionCategoria->camino;
				//$datos[ "ruta" ] = $archivo->ruta;
				$datos["etiquetas"] = $archivo->etiquetas;
				$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
				if (file_exists($miniatura)) {
					//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

					//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
					$img = new Imagick($miniatura);
					$tamaño = 384;
					if (
						$img->getHeight() > $tamaño ||
						$img->getWidth() > $tamaño
					) {
						$img->resize(
							$tamaño,
							$tamaño,
							Image::AUTO
						);
					}
					$imagen = base64_encode($img->render());
					$formato = $img->getMime();
					$datos["miniatura"] =  "data:$formato;base64,$imagen";
				}
				//$datos["carpeta"] = $archivo->ubicacionCarpeta->camino;
				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function usuario($id)
	{
		//print_r( $id );
		$datos = array();

		if ($this->request->isPost()) {
			//$r["ok"] = "post";
		} else {
			$d = Usuarios::findFirst($id);
			//$d->disco = $d->disco;
			$datos = $d->toArray([
				"id",
				"nombre",
				"rol"
			]);
			$datos["carpetas"] = $d->carpetas;
			$datos["disco"] = $d->disco;
		}
		//var_dump( $datos );

		$this->response->setJsonContent($datos);
		return $this->response;
		//return;
	}

	public function usuario_carpetas($id)
	{
		//print_r( $id );
		$datos = array();

		$usuario = Usuarios::findFirst($id);

		if ($this->request->isPost()) {
			$json = $this->request->getJsonRawBody();
			/*
			$datos = [
				"nombre" => $json->nombre,
				"propietario" => $id,
				"ruta" => sha1( $json->contenedora . $json->nombre ),
				"contenedora" => $json->contenedora
			];

			if ( isset( $json->id )) {
				$datos[ "id" ] = $json->id;
			}*/

			$carpeta = new Carpetas([
				"nombre" => $json->nombre,
				"contenedora" => $json->contenedora,
				"propietario" => $id
			]);
			//$carpeta->propietario = $id;
			//$carpeta->ruta = sha1( $json->contenedora . $json->nombre );
			//var_dump( $carpeta->dump() );

			if ($carpeta->save()) {
				$carpeta->refresh();
				$this->response->setStatusCode(200, "OK");
				$datos["ok"] = true;
				$datos["carpeta"] = $carpeta;
			} else {
				$this->response->setStatusCode(409, "Conflicto");
				$datos["ok"] = false;
				$m = $carpeta->getMessages();
				//$datos[ "mensaje" ] = $carpeta->getMessages();
				$datos["mensajes"] = [];
				foreach ($carpeta->getMessages() as $message) {
					$datos["mensajes"][] = print_r($message->getMessage(), true);
				}
			}
		} else {
			//print_r( $usuario->disco->getArbol()->subcarpetas ?  );
			$datos = $usuario->disco->getArbol()->subcarpetas;
			//$datos = Carpetas::arbol( $usuario->disco->id );
			//$datos = Carpetas::arbol( 3 );
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function usuario_archivo()
	{
		$datos = array();

		if ($this->request->isPost()) {
			$json = $this->request->getJsonRawBody(true);

			//print_r( $json );
			$su = Subidas::findById($json["id"])[0];
			//print_r( $su->toArray() );
			//return;

			$fichero = sha1($json["nombre"]);

			$archivo = new Archivos();
			$archivo->descripcion = $json["descripcion"];
			$archivo->categoria = $json["categoria"];
			$archivo->nombre = $json["nombre"];
			$archivo->tipo =  $json["ext"];
			$archivo->carpeta = $json["carpeta"];
			$archivo->archivo =  $fichero;

			if ($archivo->save()) {

				$archivo->refresh();

				//$carpeta = DISCO . "/" . sha1( $json[ "usuario" ]);
				$carpeta = $archivo->UbicacionCarpeta->raiz->ruta;
				if (!file_exists($carpeta)) {
					mkdir($carpeta);
				}

				//rename( $su->archivo, "$carpeta/$fichero." . $json["ext"] );

				$this->response->setStatusCode(200, "OK");
				$datos["ok"] = true;
				$datos["archivo"] = $archivo->toArray();

				$su->delete();
			} else {
				$this->response->setStatusCode(409, "Conflicto");
				$datos["ok"] = false;
				$datos["mensajes"] = [];
				foreach ($archivo->getMessages() as $message) {
					$datos["mensajes"][] = print_r($message->getMessage(), true);
				}
			}
		}

		$this->response->setJsonContent($r);
		return $this->response;
	}

	public function carpeta_archivos($id)
	{
		//@set_time_limit(5 * 60);
		$datos = array();
		$carpeta = Carpetas::findFirst($id);

		if ($this->request->isPost()) {
			$json = $this->request->getJsonRawBody();

			$subida = Subidas::findFirst("id = '$json->id'");

			$procede = true;
			$mensajes = [];

			if (!$subida) {
				$procede = false;
				$mensaje = "No se ha subido el archivo";
			}

			if (!file_exists($subida->archivo)) {
				$procede = false;
				$mensaje = "No se ha subido el archivo";
			}
			$progreso = $subida->progreso;
			if ($subida->progreso < 1) {
				$procede = false;
				$mensaje = "No se ha subido el archivo por completo";
			}

			// $factor = 100;
			// if ( floor( $json->largo / $factor ) != floor( filesize( $subida->archivo ) / $factor )) {
			// 	$procede = false;
			// 	$mensaje = "El tamaño del archivo no corresponde";
			// 	$datos["tc"] = $json->largo;
			// 	$datos["ts"] = filesize($subida->archivo);
			// }

			if ($procede) {
				//$p = "nombre = '$json->nombre'"
				//	." AND tipo = '$json->ext'"
				//	." AND carpeta = $id";

				$version = 1; //Archivos::count( $p ) + 1;
				$fichero = $carpeta->ruta
					. sha1($json->nombre . $version)
					. "." . $json->ext;

				$archivo = new Archivos();
				$archivo->categoria = $json->subcategoria ? $json->subcategoria : $json->categoria;
				$archivo->nombre = $json->nombre;
				$archivo->tipo =  $json->ext;
				$archivo->carpeta = $id;
				$archivo->descripcion = $json->descripcion;
				$archivo->archivo =  $fichero;
				$archivo->version = $version;

				$ids = empty($json->etiquetas->ids) ? [] : $json->etiquetas->ids;
				$nuevas = empty($json->etiquetas->nuevas) ? [] : $json->etiquetas->nuevas;
				$archivo->asignarEtiquetas($ids, $nuevas);

				//if( $archivo->save() ){
				if ($archivo->create()) {
					$archivo->refresh();

					//$datos[ "archivo" ]["ruta"] = $archivo->ruta;

					$carpeta = DISCO . "/" . $carpeta->raiz->ruta;
					//var_dump( $carpeta );
					if (!file_exists($carpeta)) {
						mkdir($carpeta);
					}

					rename($subida->archivo, $archivo->ruta);
					$subida->delete();

					$imagen = $json->imagen;
					$re = '/^data:(?<formato>[a-z\/]+);base64,(?<datos>[A-Z0-9a-z+\/]{80,}[=]{0,3})$/su';
					if (preg_match($re, $imagen, $captura)) {

						list(, $extension) = explode("/", $captura["formato"]);

						$min = DISCO . "/miniaturas/$fichero.$extension";
						file_put_contents($min, base64_decode($captura["datos"]));
					}

					$datos["ok"] = true;
					$datos["archivo"] = $archivo->toArray();
				} else {
					$this->response->setStatusCode(409, "Conflicto");
					$datos["ok"] = false;
					$m = $archivo->getMessages();
					//$datos[ "mensaje" ] = $carpeta->getMessages();
					$datos["mensajes"] = [];
					foreach ($archivo->getMessages() as $message) {
						$datos["mensajes"][] = print_r($message->getMessage(), true);
					}
				}
			} else {
				$datos["ok"] = false;
				$datos["mensajes"] = [$mensaje];
			}
		} else {
			//$datos = $carpeta->archivos;
			$archivos = $carpeta->archivos;
			foreach ($archivos as $archivo) {
				$actual = $archivo->toArray();
				$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
				if (file_exists($miniatura)) {
					//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

					//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
					$img = new Imagick($miniatura);
					$tamaño = 128;
					if (
						$img->getHeight() > $tamaño ||
						$img->getWidth() > $tamaño
					) {
						$img->resize(
							$tamaño,
							$tamaño,
							Image::AUTO
						);
					}
					$imagen = base64_encode($img->render());
					$formato = $img->getMime();
					$actual["miniatura"] =  "data:$formato;base64,$imagen";
				} else {
					//$actual[ "miniatura" ] = $miniatura;
				}
				$datos[] = $actual;
			}
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function categoria_archivos($id)
	{
		$datos = array();
		$categoria = Categorias::findFirst($id);
		//$datos = $categoria->archivos;
		//$archivos = $categoria->archivos;
		$archivos = $categoria->TodoArchivos;

		foreach ($archivos as $archivo) {
			//$actual = $archivo->toArray();
			$actual = (Object)$archivo;
			$miniatura = DISCO . "/miniaturas/$actual->archivo.jpeg";
			if (file_exists($miniatura)) {
				//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

				//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
				$img = new Imagick($miniatura);
				$tamaño = 128;
				if (
					$img->getHeight() > $tamaño ||
					$img->getWidth() > $tamaño
				) {
					$img->resize(
						$tamaño,
						$tamaño,
						Image::AUTO
					);
				}
				$imagen = base64_encode($img->render());
				$formato = $img->getMime();
				$actual->miniatura =  "data:$formato;base64,$imagen";
			} else {
				$actual->miniatura = $miniatura;
			}
			$datos[] = $actual;
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function archivo_etiquetas($id)
	{
		$datos = array();
		$archivo = Archivos::findFirst($id);
		$json = $this->request->getJsonRawBody();

		switch ($this->request->getMethod()) {
			case 'POST':

				$ids = empty($json->ids) ? [] : $json->ids;
				$nuevas = empty($json->nuevas) ? [] : $json->nuevas;
				$archivo->asignarEtiquetas($ids, $nuevas);

				if ($archivo->update()) {
					$archivo->refresh();

					$datos["ok"] = true;
					$datos["archivo"] = $archivo->toArray();
					$datos["archivo"]["etiquetas"] = $archivo->etiquetas;
				} else {
					$this->response->setStatusCode(400, "Error");
					$datos["ok"] = false;
					$m = $archivo->getMessages();
					//$datos[ "mensaje" ] = $carpeta->getMessages();
					$datos["mensajes"] = [];
					foreach ($archivo->getMessages() as $message) {
						$datos["mensajes"][] = print_r($message->getMessage(), true);
					}
				}
				break;
			case 'GET':
				$datos = $archivo->getRelated("etiquetas", [
					"columns" => "id, nombre"
				]);
				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}



		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function archivo_descargar($id)
	{
		$datos = array();
		$archivo = Archivos::findFirst($id);
		$json = $this->request->getJsonRawBody();

		switch ($this->request->getMethod()) {
			case 'GET':
				if (file_exists($archivo->ruta)) {
					//$finfo = new finfo( FILEINFO_MIME_TYPE );
					//$tipo = $finfo->file($archivo->ruta);
					//$tipo = mime_content_type($archivo->ruta);
					$peso = filesize($archivo->ruta);

					//var_dump( $archivo->nombreCompleto );
					$this->response->setContentLength($peso);
					//$this->response->setContentType( $tipo );
					$this->response->setFileToSend($archivo->ruta, $archivo->nombreCompleto, true);
					return $this->response;
				}
				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function etiquetas()
	{
		$datos = array();
		$etiquetas = Etiquetas::find([
			"columns" => "id,nombre",
			"conditions" => "grupo='normal' AND cuenta > 0",
			"order" => "cuenta DESC",
			"limit" => 20,
		]);

		$etiquetas = $etiquetas->toArray();


		$datos = array_combine(
			array_column($etiquetas, "id"),
			array_column($etiquetas, "nombre")
		);

		//$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function todasEtiquetas()
	{
		$datos = array();
		$etiquetas = Etiquetas::find([
			"columns" => "id,nombre",
			"conditions" => "grupo='normal' AND cuenta > 0",
			"order" => "nombre",
		]);

		$etiquetas = $etiquetas->toArray();

		//$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($etiquetas);
		return $this->response;
	}


	public function buscar(Int $categoria = 1)
	{
		$t = $this->request->get("v");

		//$this->response->setJsonContent( $t );
		//return $this->response;

		//$phpql = "SELECT * FROM Docbox\Model\Archivos WHERE MATCH (nombre,descripcion)
		//	AGAINST ( '*:tt:*' )";

		//$consulta = $this->modelsManager->createQuery( $phpql );
		//$datos = $consulta->execute([ "tt" => $t ]);

		//$filtroCategoria = ( $categoria != 1 ) ? "AND categoria = $categoria" : "";
		//$listaCategorias = [ $categoria ];

		if ($categoria != 1) {
			// $cat = Categorias::findFirst( $categoria );
			// print_r( $categoria->Supercategoria->descendientes->toArray() );

			/*
			if($cat->Supercategoria->id != 1){
				$listaCategorias = [ $cat->Supercategoria->id ];

				// $categoria->Supercategoria->descendientes->toArray());
				// array_merge($listaCategorias, );
				// print_r($l);
				$listaCategorias = array_merge($listaCategorias, array_map( function( $e ){
						return $e["id"];
					},
					$cat->Supercategoria->descendientes->toArray()));
			}else{
				$listaCategorias = [ $categoria ];
			}
			*/
			$listaCategorias = [$categoria];
			$cat = Categorias::findFirst($categoria);
			$listaCategorias = array_merge(
				$listaCategorias,
				array_map(
					function ($e) {
						return $e["id"];
					},
					$cat->descendientes->toArray()
				)
			);


			$filtroCategoria = sprintf("AND categoria IN (%s)", implode($listaCategorias, ","));
		} else {
			$filtroCategoria = "";
		}

		/*
		$sql = "SELECT
				d.id,
				d.nombre,
				d.descripcion,
				d.categoria,
				d.tipo
			FROM documento d
			WHERE
				MATCH( d.nombre, d.descripcion ) AGAINST( '*$t*' IN BOOLEAN MODE)
				OR d.tipo LIKE '%$t%'
				$filtroCategoria
			LIMIT 100";
		*/
		$sql = "SELECT
					d.id,
					d.nombre,
					d.descripcion,
					d.archivo,
					d.categoria,
					d.tipo
				FROM documento d
				WHERE
					(
						MATCH( d.nombre, d.descripcion ) AGAINST( '*$t*' IN BOOLEAN MODE)
						OR d.tipo LIKE '%$t%'
						OR d.id IN ( SELECT
									de.documento
								FROM documento_etiqueta de
									JOIN etiqueta e
									ON de.etiqueta = e.id
								WHERE
									MATCH( e.nombre ) AGAINST( '*$t*' IN BOOLEAN MODE))
					) $filtroCategoria
				LIMIT 1000";

		//var_dump($sql);
		//return;

		$bd = $this->db;
		$rs = $bd->query($sql);
		$rs->setFetchMode(\Phalcon\Db::FETCH_OBJ);

		$datos = [];
		//$rs->fetchAll();
		while ($archivo = $rs->fetch()) {
			$obj = Archivos::findFirst($archivo->id);
			$archivo->categoria = $obj->ubicacionCategoria;
			$archivo->etiquetas = $obj->etiquetas;
			$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
			if (file_exists($miniatura)) {
				//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

				//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
				$img = new Imagick($miniatura);
				$tamaño = 128;
				if (
					$img->getHeight() > $tamaño ||
					$img->getWidth() > $tamaño
				) {
					$img->resize(
						$tamaño,
						$tamaño,
						Image::AUTO
					);
				}
				$imagen = base64_encode($img->render());
				$formato = $img->getMime();
				$archivo->miniatura =  "data:$formato;base64,$imagen";
			}
			$datos[] = $archivo;
		}


		$this->response->setJsonContent($datos);
		return $this->response;
	}


	public function usuario_favoritos(Int $id)
	{
		$datos = array();
		$usuario = Usuarios::findFirst($id);
		$json = $this->request->getJsonRawBody();

		switch ($this->request->getMethod()) {
			case "POST":
				//print_r( $json );
				if ($json->marcar) {

					$favorito = new Favoritos([
						"usuario" => $id,
						"documento" => $json->id
					]);
					$favorito->save();
					$datos["ok"] = true;
				} else {
					$favorito = Favoritos::findFirst("usuario = $id AND documento = $json->id");
					//$datos = $favorito;

					$favorito->delete();
					$datos["ok"] = true;
				}
				break;
			case 'GET':
				$datos = $usuario->getFavoritos();
				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function archivo_previo(Int $id)
	{
		$archivo = Archivos::findFirst($id);
		$tiposGráficos = [
			"png",
			"jpg",
		];
		$tiposAnimaciones = [
			"gif",
			"mp4",
		];

		$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";

		if (in_array($archivo->tipo, $tiposGráficos)) {
			$img = new Imagick($archivo->ruta);
			$tamaño = 640;
			if (
				$img->getHeight() > $tamaño ||
				$img->getWidth() > $tamaño
			) {
				$img->resize(
					$tamaño,
					$tamaño,
					Image::AUTO
				);
			}

			$this->response->setContentType($img->getMime());
			$this->response->setContent($img->render());
		} elseif (in_array($archivo->tipo, $tiposAnimaciones)) {
			$peso = filesize($archivo->ruta);
			$tipo = mime_content_type($archivo->ruta);

			$this->response->setContentLength($peso);
			$this->response->setContentType($tipo);
			$this->response->setFileToSend($archivo->ruta, $archivo->nombreCompleto, false);
		} elseif (file_exists($miniatura)) {
			$peso = filesize($miniatura);
			$tipo = mime_content_type($miniatura);

			$this->response->setContentLength($peso);
			$this->response->setContentType($tipo);
			$this->response->setFileToSend($miniatura, $archivo->nombreCompleto, false);
		} else {
			$this->response->setContentType("text/plain", "UTF-8");
			$this->response->setContent("Este archivo no tiene imagen previa");
		}
		return $this->response;
	}

	public function archivo_miniatura(Int $id)
	{
		$archivo = Archivos::findFirst($id);
		$tiposGráficos = [
			"png",
			"jpg",
			"gif"
		];

		//$nombreArchivo = str_replace( ".", "-", $archivo->archivo );
		$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
		//echo $nombreArchivo . PHP_EOL;
		//echo $miniatura . PHP_EOL;
		//echo PHP_EOL;
		if (file_exists($miniatura)) {
			//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
			$img = new Imagick($miniatura);
			$tamaño = 128;
			if (
				$img->getHeight() > $tamaño ||
				$img->getWidth() > $tamaño
			) {
				$img->resize(
					$tamaño,
					$tamaño,
					Image::AUTO
				);
			}
			$imagen = base64_encode($img->render());
			$formato = $img->getMime();
			//var_dump( $img->getMime() );
			//$this->response->setContentType( $img->getMime() );
			//$this->response->setContent( $img->render() );
			$this->response->setContent("data:$formato;base64,$imagen");
		} else {
			//echo $nombreArchivo;
		}

		/*
		if( in_array( $archivo->tipo, $tiposGráficos )){
			$img = new Imagick( $archivo->ruta );
			$tamaño = 128;
			if( $img->getHeight() > $tamaño ||
				$img->getWidth() > $tamaño )
			{
				$img->resize(
					$tamaño,
					$tamaño,
					Image::AUTO
				);
			}

			$this->response->setContentType( $img->getMime() );
			$this->response->setContent( $img->render() );
		}
		else{
			$this->response->setContentType("text/plain", "UTF-8");
			$this->response->setContent( "Este archivo no tiene imagen previa" );
		}
		*/
		return $this->response;
	}

	public function usuario_perfil(Int $id)
	{
		$datos = array();
		$usuario = Usuarios::findFirst($id);
		$json = $this->request->getJsonRawBody();

		switch ($this->request->getMethod()) {
			case "POST":
				$usuario->correo = $json->correo;
				$usuario->perfil->nombre = $json->nombre;
				$usuario->perfil->apellidos = $json->apellidos;
				$usuario->perfil->telefono = $json->telefono;
				$usuario->perfil->imagen = $json->imagen;

				if (!empty($json->clave)) {
					$usuario->clave = $json->clave;
				}

				$usuario->save();
				$datos["ok"] = true;
				break;
			case 'GET':
				//var_dump( $usuario->perfil->dump() );
				$datos = $usuario->perfil;
				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setJsonContent($datos);
		return $this->response;
	}

	public function carpeta_enlace($id)
	{
		$datos = [];

		$clave = md5($this->config["authentication"]->secret);
		//$criptex = new Crypt();
		$token = $this->criptex->encryptBase64($id, null, true);

		$datos["url"] = sprintf("%s%scompartidos/carpeta/%s", WEB, $this->url->getBaseUri(),  urlencode($token));


		$this->response->setJsonContent($datos);
		return $this->response;
		//print_r($_SERVER);
	}
	public function archivo_enlace($id)
	{
		$datos = [];

		$clave = md5($this->config["authentication"]->secret);
		//$criptex = new Crypt();
		$token = $this->criptex->encryptBase64($id, null, true);

		$datos["url"] = sprintf("%s%scompartidos/archivo/%s", WEB, $this->url->getBaseUri(),  urlencode($token));


		$this->response->setJsonContent($datos);
		return $this->response;
		//print_r($_SERVER);
	}

	public function plupload()
	{
		@set_time_limit(5 * 60);
		//echo sys_get_temp_dir();
		//exit();
		if (true == $this->request->hasFiles() && $this->request->isPost()) {
			$fichero = sha1($this->request->getPost("name"));
			$id = $this->request->getPost("id");
			$usuario = $this->request->getPost("usuario");
			$temp = PUBLICA . "/temp/" . sha1($usuario) . $fichero;
			$file = $this->request->getUploadedFiles()[0];
			//var_dump($file);

			$file->moveTo($temp);

			$su = new Subidas([
				"id" => $id,
				"archivo" => $temp,
				"avance" => "1 - 1",
				"usuario" => $usuario
			]);
			$su->save();
			$this->response->setStatusCode(200, utf8_decode("archivo guardado"));
			$this->response->setJsonContent([
				"OK" => true,
				"info"=> "Upload successful."
			]);
			return $this->response;
		}
	}
}
