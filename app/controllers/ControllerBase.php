<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
	protected function renderizaTexto( $obj ){
		$this->response->setContentType("text/plain", "UTF-8");
		$this->response->setContent( print_r($obj, true ));
		return $this->response;
	}
}
