<?php

use Phalcon\Image;
use \Phalcon\Crypt;

use Docbox\Model\Archivos;
use Docbox\Model\Carpetas;
use Phalcon\Image\Adapter\Gd;
use Phalcon\Image\Adapter\Imagick;
//use Docbox\Model\Archivos;

class CompartidosController extends ControllerBase
{
	protected $ip_seguro = [
		"127.0.0.1",
		"201.240.145.11"
	];

	public function index()
	{ }

	public function carpeta_id($id)
	{
		if (in_array($this->request->getClientAddress(), $this->ip_seguro)) {

			$carpeta = Carpetas::findFirst($id);
			if (!$carpeta) {
				$this->response->setStatusCode(404, "Not Found")->sendHeaders();
				//return $this->view->render('404');
			}

			$this->view->carpeta = (Object)$carpeta->toArray();
			$archivos = [];

			foreach ($carpeta->archivos as $archivo) {
				$actual = $archivo->toArray();
				$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
				if (file_exists($miniatura)) {
					//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

					//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
					$img = new Imagick($miniatura);
					$tamaño = 256;
					if (
						$img->getHeight() > $tamaño ||
						$img->getWidth() > $tamaño
					) {
						$img->resize(
							$tamaño,
							$tamaño,
							Image::INVERSE
						);
						$img->crop(
							$tamaño,
							$tamaño
						);
					}
					$imagen = base64_encode($img->render());
					$formato = $img->getMime();
					$actual["miniatura"] =  "data:$formato;base64,$imagen";
				} else {
					$actual["miniatura"] = false;
				}
				$archivos[] = (Object)$actual;
			}
			$this->view->archivos = $archivos;

			return $this->view->render("compartir-carpeta");
		}
	}


	public function carpeta($token)
	{
		$clave = md5($this->config["authentication"]->secret);
		$token = urldecode($token);
		$id = $this->criptex->decryptBase64($token, null, true);

		$carpeta = Carpetas::findFirst($id);
		$this->view->carpeta = (Object)$carpeta->toArray();
		$archivos = [];

		foreach ($carpeta->archivos as $archivo) {
			$actual = $archivo->toArray();
			$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
			if (file_exists($miniatura)) {
				//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

				//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
				$img = new Imagick($miniatura);
				$tamaño = 256;
				if (
					$img->getHeight() > $tamaño ||
					$img->getWidth() > $tamaño
				) {
					$img->resize(
						$tamaño,
						$tamaño,
						Image::INVERSE
					);
					$img->crop(
						$tamaño,
						$tamaño
					);
				}
				$imagen = base64_encode($img->render());
				$formato = $img->getMime();
				$actual["miniatura"] =  "data:$formato;base64,$imagen";
			} else {
				$actual["miniatura"] = false;
			}
			$archivos[] = (Object)$actual;
		}
		$this->view->archivos = $archivos;

		return $this->view->render("compartir-carpeta");
		// M3Zd8XqlYOujZ7cyQRghGe8=
	}

	public function archivo($token)
	{
		$clave = md5($this->config["authentication"]->secret);
		$token = urldecode($token);
		$id = $this->criptex->decryptBase64($token, null, true);

		$archivo = Archivos::findFirst($id);
		$actual = $archivo->toArray();

		$miniatura = DISCO . "/miniaturas/$archivo->archivo.jpeg";
		if (file_exists($miniatura)) {
			//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";

			//$actual[ "miniatura" ] =  DISCO . "/miniaturas/$archivo->archivo.webp";
			$img = new Imagick($miniatura);
			$tamaño = 640;
			if (
				$img->getHeight() > $tamaño ||
				$img->getWidth() > $tamaño
			) {
				$img->resize(
					$tamaño,
					$tamaño,
					Image::INVERSE
				);
				$img->crop(
					$tamaño,
					$tamaño
				);
			}
			$imagen = base64_encode($img->render());
			$formato = $img->getMime();
			$actual["miniatura"] =  "data:$formato;base64,$imagen";
		} else {
			$actual["miniatura"] = false;
		}
		$this->view->archivo = (Object)$actual;
		$this->view->token = urlencode($token);


		return $this->view->render("compartir-archivo");
		// M3Zd8XqlYOujZ7cyQRghGe8=
	}

	public function archivo_descargar($token)
	{
		$clave = md5($this->config["authentication"]->secret);
		$token = urldecode($token);
		$id = $this->criptex->decryptBase64($token, null, true);

		$datos = array();
		$archivo = Archivos::findFirst($id);

		switch ($this->request->getMethod()) {
			case 'GET':
				if (file_exists($archivo->ruta)) {
					//$finfo = new finfo( FILEINFO_MIME_TYPE );
					//$tipo = $finfo->file($archivo->ruta);
					//$tipo = mime_content_type($archivo->ruta);
					$peso = filesize($archivo->ruta);

					//var_dump( $archivo->nombreCompleto );
					$this->response->setContentLength($peso);
					//$this->response->setContentType( $tipo );
					$this->response->setFileToSend($archivo->ruta, $archivo->nombreCompleto, true);
					return $this->response;
				}
				break;
			default:
				$this->response->setStatusCode(405, utf8_decode("Método no permitido"));
				$datos["ok"] = false;
				break;
		}

		$this->response->setContentType("application/json", "UTF-8");
		$this->response->setJsonContent($datos);
		return $this->response;
	}
}
