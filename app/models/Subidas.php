<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;

class Subidas extends Model
{
	public function initialize()
	{
		$this->setSource('subida');

		$this->skipAttributes(
			[
				'timestamp'
			]
		);

		$this->belongsTo( "usuario", "Docbox\Model\Usuarios", "id", [
			"alias" => "propietario"
		]);

	}

	public function getProgreso()
	{
		$v = explode( " - ", $this->avance );
		return $v[0] / $v[1];
	}



}
