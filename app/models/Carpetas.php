<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\PresenceOf;

class Carpetas extends Model
{
	public function initialize()
	{
		$this->setSource('carpeta');

		$this->skipAttributes(
			[
				'creada',
				'modificada',
			]
		);

		$this->hasMany("id", "Docbox\Model\Carpetas", "contenedora", [
			'alias' => 'Subcarpetas',
		]);
		$this->hasMany("id", "Docbox\Model\Archivos", "carpeta", [
			'alias' => 'Archivos',
		]);

		$this->belongsTo( "contenedora", "Docbox\Model\Carpetas", "id", [
			'alias' => 'Supercarpeta',
			'params'   => [
				'conditions' => "id > 1"
			]
		]);

		$this->belongsTo( "propietario", "Docbox\Model\Usuarios", "id", [
			'alias' => 'Usuario',
		]);
	}

	public function validation(){

		$validador = new Validation();
		$validador->add(
			[ "contenedora", "nombre" ],
			new Uniqueness(
				[
					'message' => 'El nombre debe ser único'
				]
			)
		);

		return $this->validate($validador);
	}

	public function beforeSave()
	{
		if( empty( $this->ruta )){
			$this->ruta = sha1( "$this->contenedora.$this->nombre" );
		}

	}

	public function getDescendientes()
	{
		$arbol = Categorias::findByContenedora( $this->id );

		return $arbol;
	}

	public function getAncestro()
	{
		//$ancestro = Categorias::findFirstById( $this->contenedora );

		return $this->SuperCarpeta;//Categorias::findFirstById( $this->contenedora );
	}

	public function getArbol(){
		return self::arbol( $this->id );
	}

	public function getCamino(){
		return  self::camino( $this->id );
	}

	public function getRaiz(){
		return  $this->usuario->disco;
	}

	public static function arbol( $id = null ){
		$arbol = [];

		$raiz = self::findFirst( $id );
		if ($raiz) {
			$arbol = $raiz->toArray([ "id", "nombre" ]);
			$arbol["subcarpetas"] = [];

			/*
			foreach ($raiz->getDescendientes() as $obj ) {
				$arbol[ "descendientes" ][] = self::arbol( $obj->id );
			}
			*/
			foreach ($raiz->getSubcarpetas() as $obj ) {
				$arbol[ "subcarpetas" ][] = self::arbol( $obj->id );
			}
		}
		return (Object) $arbol;
	}

	public static function camino( $id = null ){
		$camino = [];

		$origen = self::findFirst( $id );
		if ($origen) {
			//$camino = $origen->toArray();
			//$camino[ "ancestros" ] = [];
			$actual = $origen->ancestro;
			while ($actual) {
				if( $actual->id != $actual->raiz->id) {
					$camino[] = $actual->toArray([ "id", "nombre" ]);
				}
				$actual = $actual->ancestro;
			}
		}
		return array_reverse( $camino );

	}

}
