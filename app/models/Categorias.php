<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;

class Categorias extends Model
{
	public function initialize()
	{
		$this->setSource('categoria');

		$this->skipAttributes(
			[
				'creada',
				'modificada',
			]
		);

		$this->hasMany("id", "Docbox\Model\Categorias", "contenedora", [
			'alias' => 'Subcategorias',
		]);
		$this->hasMany("id", "Docbox\Model\Archivos", "categoria", [
			'alias' => 'archivos',
		]);

		$this->belongsTo("contenedora", "Docbox\Model\Categorias", "id", [
			'alias' => 'Supercategoria',
		]);
	}

	public function getDescendientes()
	{
		//$arbol = Categorias::findByContenedora( $this->id );
		$arbol = Categorias::find([
			"conditions" => "contenedora = ?1 AND eliminada = 0",
			"bind" => [
				1 => $this->id
			]
		]);

		return $arbol;
	}

	public function getAncestro()
	{
		//$ancestro = Categorias::findFirstById( $this->contenedora );

		return Categorias::findFirstById($this->contenedora);
	}

	public function getArbol()
	{
		return  self::arbol($this->id);
	}

	public function getCamino()
	{
		return  self::camino($this->id);
	}

	public function getTodoArchivos()
	{
		$archivos = $this->getArchivos()->toArray();
		$subs = $this->getDescendientes();
		foreach ($subs as $subcat) {
			$archivos = array_merge($archivos, $subcat->getArchivos()->toArray());
		}

		return $archivos;
	}

	public static function arbol($id = null)
	{
		$arbol = [];

		$raiz = self::findFirst($id);
		if ($raiz) {
			$arbol = $raiz->toArray();
			$ruta["descendientes"] = [];
			foreach ($raiz->getDescendientes() as $obj) {
				//$obj->camino = $obj->camino;
				$arbol["descendientes"][] = self::arbol($obj->id);
			}
		}
		return $arbol;
	}

	public static function camino($id = null)
	{
		$camino = [];

		$origen = self::findFirst($id);
		if ($origen) {
			$camino = $origen->toArray();
			$camino["ancestros"] = [];
			$actual = $origen->ancestro;
			while ($actual) {
				$camino["ancestros"][] = $actual->toArray();
				$actual = $actual->ancestro;
			}
		}
		return $camino;
	}
}
