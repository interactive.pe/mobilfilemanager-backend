<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;

class Etiquetas extends Model
{
	public function initialize()
	{
		$this->setSource('etiqueta');

		$this->skipAttributes(
			[
				'creada',
			]
		);

		$this->hasManyToMany(
			"id",
			"Docbox\Model\ArchivosEtiquetas",
			"etiqueta", "documento",
			"Docbox\Model\Archivos",
			"id",
			[
				"alias" => "archivos",
			]
		);
	}

	public function validation(){

		$validador = new Validation();
		$validador->add(
			[ "grupo", "nombre", "valor" ],
			new Uniqueness(
				[
					'message' => 'El nombre debe ser único'
				]
			)
		);
		return $this->validate($validador);
	}

}
