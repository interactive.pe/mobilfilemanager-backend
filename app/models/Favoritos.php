<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;

class Favoritos extends Model
{
	public function initialize()
	{
		$this->setSource('favorito');

		$this->skipAttributes(
			[
				'creado',
			]
		);

		$this->belongsTo( "usuario", "Docbox\Model\Carpetas", "id" );

		$this->hasMany(
			"id",
			"Docbox\Model\Archivos",
			"documento"
		);
	}

}
