<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;

class ArchivosEtiquetas extends Model
{
	public function initialize()
	{
		$this->setSource('documento_etiqueta');

		$this->skipAttributes(
			[
				'creada',
			]
		);

		$this->belongsTo(
			'documento',
			'Docbox\Model\Archivos',
			'id',
			[
				"alias" => "archivos",
				'reusable' => true
			]
		);

		$this->belongsTo(
			'etiqueta',
			'Docbox\Model\Etiquetas',
			'id',
			[
				"alias" => "etiquetas",
				'reusable' => true
			]
		);
	}

	public function afterSave()
	{
		$this->etiquetas->cuenta = ArchivosEtiquetas::count([
			"etiqueta" => $this->etiqueta
		]);
	}


}
