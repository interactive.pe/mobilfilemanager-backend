<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;

class Usuarios extends Model
{
	public function initialize()
	{
		$this->setSource('usuario');

		$this->skipAttributes(
			[
				'creado',
			]
		);

		$this->hasOne( "id", "Docbox\Model\Carpetas", "propietario", [
			"reusable" => true,
			"alias" => "disco",
			"order" => "contenedora ASC",
			'params'   => [
				"conditions" => "contenedora = 1",
			]
		]);

		$this->hasMany( "id", "Docbox\Model\Carpetas", "propietario", [
			"reusable" => true,
			"alias" => "carpetas",
			"order" => "contenedora ASC",
			'params'   => [
				"conditions" => "contenedora > 1 AND ",
			]
		]);

		$this->hasManyToMany(
			"id",
			"Docbox\Model\Favoritos",
			"usuario", "documento",
			"Docbox\Model\Archivos",
			"id",
			[
				"alias" => "favoritos",
				"params" => [
					"columns" => "id, nombre, descripcion, tipo"
				]
			]
		);

		$this->hasOne( "id", "Docbox\Model\Perfiles", "usuario", [
			"reusable" => true,
			"alias" => "perfil"
		]);

	}

}
