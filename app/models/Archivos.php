<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;
use Phalcon\Validation;
use Phalcon\Validation\Validator\Uniqueness;
use Phalcon\Validation\Validator\PresenceOf;

class Archivos extends Model
{
	public function initialize()
	{
		$this->setSource('documento');

		$this->skipAttributes(
			[
				'creado',
				'modificado',
			]
		);

		$this->belongsTo( "categoria", "Docbox\Model\Categorias", "id", [
			"alias" => "UbicacionCategoria"
		]);
		$this->belongsTo( "carpeta", "Docbox\Model\Carpetas", "id", [
			"alias" => "UbicacionCarpeta"
		]);

		$this->hasManyToMany(
			"id",
			"Docbox\Model\ArchivosEtiquetas",
			"documento", "etiqueta",
			"Docbox\Model\Etiquetas",
			"id",
			[
				"alias" => "etiquetas",
			]
		);

		$this->hasMany(
			"id",
			"Docbox\Model\ArchivosEtiquetas",
			"documento",
			[
				"alias" => "etiquetados",
			]
		);
	}


	public function validation(){

		$validador = new Validation();
		$validador->add(
			[ "carpeta", "nombre", "tipo", "version" ],
			new Uniqueness(
				[
					'message' => 'El nombre debe ser único'
				]
			)
		);
		return $this->validate($validador);
	}

	public function getRuta(){

		$r = [
			DISCO,
			$this->ubicacionCarpeta->raiz->ruta,
			$this->archivo
		];

		return implode( "/", $r );
	}

	public function getNombreCompleto(){
		return "$this->nombre.$this->tipo";
	}

	public function asignarEtiquetas( $ids = [], $nombres = [] ){

		$etiquetasActuales = $this->getRelated( "etiquetas",[
			"columns" => "id"
		]);

		//$actuales = array_column( $etiquetasActuales->toArray(), "id" );
		$actuales = array_map( function( $obj ){
			return (int)$obj["id"];
		}, $etiquetasActuales->toArray());

		$eliminar = array_values( array_diff( $actuales, $ids ));
		$agregar = array_values( array_diff( $ids, $actuales ));

		if( $eliminar ){
			$this->getRelated( "etiquetados",[
				"conditions" => "etiqueta in (" . implode(",",$eliminar) . ")"
			])->delete();
		}

		$nuevas = [];
		foreach ( $agregar as $etiqueta ) {
			$nuevas[] = Etiquetas::findFirst( $etiqueta );
		}

		foreach ( $nombres as $nombre ) {
			$nueva =  Etiquetas::findFirst( "nombre='$nombre' AND grupo='normal'" );
			$nuevas[] = $nueva ? $nueva : new Etiquetas([
				"nombre" => $nombre,
				"grupo" => "normal"
			]);
		}
		$this->etiquetas = $nuevas;

		return true;
	}

}
