<?php
namespace Docbox\Model;

use Phalcon\Text;
use Phalcon\Mvc\Model;

class BaseModelo extends Model
{

	public $columnas_virtuales = [];

	public function toArray( $columnas = null ) {
		$datos = parent::toArray($columnas);

		// then gets the model's virtual fields, if any
		$virtuales = [];
		if ( !empty( $this->columnas_virtuales )) {
			foreach ( $this->columnas_virtuales as $col ) {
				$getter_name = 'get' . Text::camelize( $col );
				$virtuales[ $col ] = $this->{$getter_name}();
			}
		}

		$datos = array_merge($datos, $virtuales);

		return $datos;
	}



}
