<?php
namespace Docbox\Model;

use Phalcon\Mvc\Model;

class Perfiles extends Model
{
	public function initialize()
	{
		$this->setSource('perfil');
		$this->skipAttributes(
			[
				'creado',
			]
		);

		$this->belongsTo( "usuario", "Docbox\Model\Usuario", "id" );

	}

}
