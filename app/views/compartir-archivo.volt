<!DOCTYPE html>
<html lang="es">

	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Repositorio Mobil | {{ archivo.nombre | upper }}</title>

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" />
		<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/7.0.3/sanitize.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sanitize.css/2.0.0/sanitize.min.css"/> -->

		<link rel="stylesheet" href="/css/compartir.css">
	</head>

	<body>
		<header>
			<h1>
				<figure>
					<img src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9Im5vIj8+PCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj48c3ZnIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHZpZXdCb3g9IjAgMCAxMTIgMzIiIHZlcnNpb249IjEuMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgeG1sbnM6c2VyaWY9Imh0dHA6Ly93d3cuc2VyaWYuY29tLyIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoxLjQxNDIxOyI+PGc+PHBhdGggZD0iTTIxLjQ3MSwwbC01Ljc3MSwxOS4zNDVsLTAuMDg2LDBsLTUuNjM3LC0xOS4zNDVsLTkuOTc3LDBsMCwzMC44NTJsNi40MTgsMGwwLC0yMi4yNDNsMC4wODYsMGw2LjA3MiwyMi4yNDNsNS44NTcsMGw2LjIwNSwtMjIuMjQzbDAuMDg0LDBsMCwyMi4yNDNsNi40MjYsMGwwLC0zMC44NTJsLTkuNjc3LDBaIiBzdHlsZT0iZmlsbDojMDA2MmFlO2ZpbGwtcnVsZTpub256ZXJvOyIvPjxwYXRoIGQ9Ik00NS41OTMsMjQuMzdjMi45NTEsMCA1LjM3OSwtMi40MzkgNS4zNzksLTUuMzI0YzAsLTIuOTczIC0yLjQyOCwtNS40MSAtNS4zNzksLTUuNDFjLTIuOTUxLDAgLTUuMzgsMi40MzcgLTUuMzgsNS40MWMwLDIuODg1IDIuNDI5LDUuMzI0IDUuMzgsNS4zMjRtMCwtMTcuMTI3YzYuNTA2LDAgMTEuNzk4LDUuMjQ4IDExLjc5OCwxMS44MDNjMCw2LjQ2OCAtNS4yOTIsMTEuODA2IC0xMS43OTgsMTEuODA2Yy02LjUxMSwwIC0xMS44LC01LjMzOCAtMTEuOCwtMTEuODA2YzAsLTYuNTU1IDUuMjg5LC0xMS44MDMgMTEuOCwtMTEuODAzIiBzdHlsZT0iZmlsbDojZTQwNTIwO2ZpbGwtcnVsZTpub256ZXJvOyIvPjxwYXRoIGQ9Ik03MS45MjQsMTMuOTQ3Yy0zLjA4LDAgLTUuNTUsMi41NTIgLTUuNTUsNS42MzRjMCwzLjA4NiAyLjQ3LDUuNjMzIDUuNTUsNS42MzNjMy4wNzgsMCA1LjU1MywtMi41NDcgNS41NTMsLTUuNjMzYzAsLTMuMDgyIC0yLjQ3NSwtNS42MzQgLTUuNTUzLC01LjYzNG0tMTEuOTc2LDE2LjkwM2wwLC0zMC44NWw2LjQyNiwwbDAsOS42MDRjMS44NjMsLTEuMTggMy45NDIsLTEuODI2IDYuMTU2LC0xLjgyNmM2LjQ2NiwwIDExLjM2NSw1LjM1NiAxMS4zNjUsMTEuODAzYzAsNi4zNTUgLTQuODk5LDExLjgwNiAtMTEuMzY1LDExLjgwNmMtMy42NDQsMCAtNS4zOCwtMS43MTQgLTYuMTU2LC0yLjYzOGwwLDIuMTAxbC02LjQyNiwwWiIgc3R5bGU9ImZpbGw6IzAwNjJhZTtmaWxsLXJ1bGU6bm9uemVybzsiLz48cGF0aCBkPSJNODYuNSw4LjMxMWw2LjQxOCwwbDAsMjIuNTM5bC02LjQxOCwwbDAsLTIyLjUzOVptMCwtOC4zMTFsNi40MTgsMGwwLDYuMTc0bC02LjQxOCwwbDAsLTYuMTc0WiIgc3R5bGU9ImZpbGw6IzAwNjJhZTtmaWxsLXJ1bGU6bm9uemVybzsiLz48cmVjdCB4PSI5Ni44NjgiIHk9IjAiIHdpZHRoPSI2LjQxOCIgaGVpZ2h0PSIzMC44NTEiIHN0eWxlPSJmaWxsOiMwMDYyYWU7Ii8+PHBhdGggZD0iTTEwNy4zOTksMy4wODVsLTAuNjQzLDBsMCwtMi41MjlsLTAuOTMyLDBsMCwtMC41NTZsMi41MDYsMGwwLDAuNTU2bC0wLjkzMSwwbDAsMi41MjlaIiBzdHlsZT0iZmlsbDojMDA2MmFlO2ZpbGwtcnVsZTpub256ZXJvOyIvPjxwYXRoIGQ9Ik0xMTEuNjY5LDMuMDg1bC0wLjU5OSwwbDAsLTIuNTYzbC0wLjAxLDBsLTAuNTc0LDIuNTYzbC0wLjYzMiwwbC0wLjU2MiwtMi41NjNsLTAuMDA5LDBsMCwyLjU2M2wtMC42LDBsMCwtMy4wODVsMC45NCwwbDAuNTU5LDIuNDM4bDAuMDA3LDBsMC41NDksLTIuNDM4bDAuOTMxLDBsMCwzLjA4NVoiIHN0eWxlPSJmaWxsOiMwMDYyYWU7ZmlsbC1ydWxlOm5vbnplcm87Ii8+PC9nPjwvc3ZnPg=="
						alt="Mobil" width="112" height="32">
				</figure>
			</h1>
			<h2>Archivo: <strong>{{ archivo.nombre | upper }}</strong></h2>
		</header>
		<main>
			<section class="contenido">

				<figure class="previo">
					{% if( archivo.miniatura ) %}
					<img src="{{archivo.miniatura}}" alt="">
					{%else%}
					<img src="/img/ico-img.svg" alt="">
					{% endif %}
				</figure>
				<article class="detalle">
					<h4>{{archivo.nombre}}.{{archivo.tipo}}</h4>
					<p>{{archivo.descripcion}}</p>
				</article>
				<article class="acciones">
					<a href="/compartidos/archivo/{{token}}/descargar" href="Descargar" class="descargar">
						Descargar
					</a>
				</article>
			</section>
		</main>
		<footer>
			<p class="creditos">2019&copy; Mobil</p>
		</footer>
	</body>

	<script src="/js/compartir.js"></script>

</html>
