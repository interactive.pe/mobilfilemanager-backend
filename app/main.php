<?php
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Micro;
use Phalcon\Events\Manager;

define( "DISCO", PUBLICA . "/disco" );
define( "WEB", urlBase() );

try {
	$di = new FactoryDefault();
	include APP . '/config/servicios.php';
	$config = $di->getConfig();
	include APP . '/config/cargador.php';

	$eventsManager = new Manager();
	$app = new Micro( $di );

	//$cortaFuegos = new Cortafuegos();
	$eventsManager->attach( 'micro', new Cortafuegos() );
	$app->before(new Cortafuegos() );
	$app->setEventsManager( $eventsManager );

	include APP . '/rutas/main.php';
	//$app->setService('router', $router, true);

	$app->handle();

} catch ( \Exception $e ) {
	echo $e->getMessage() . '<br>';
	echo '<pre>' . $e->getTraceAsString() . '</pre>';
}


function urlBase(){
	$protocolo = empty( $_SERVER[ 'HTTPS' ] ) ? 'http' : 'https';

	$dominio = $_SERVER[ 'SERVER_NAME' ];

	$puerto = $_SERVER['SERVER_PORT'];
	$puerto = ($protocolo == 'http' && $puerto == 80 || $protocolo == 'https' && $puerto == 443) ? '' : ":$puerto";

	return "${protocolo}://${dominio}${puerto}";
}
