<?php

use Phalcon\Di;


class Urls
{
	public function urlBase(){ return Urls::_urlBase(); }

	public static function _urlBase()
	{

		$request = Di::getDefault()->getApplication()->request;

		// server protocol
		$protocolo = $request->getScheme();

		// domain name
		$dominio = $request->getHttpHost();

		// base url
		//$base_url = preg_replace("!^${doc_root}!", '', $base_dir);

		// server port
		$puerto = $request->getPort();
		$puerto = (( $protocolo == 'http' && $puerto == 80 )
		|| ( $protocolo == 'https' && $puerto == 443 )) ? '' : ":$puerto";

		// put em all together to get the complete base URL
		$url = "${protocolo}://${dominio}${puerto}";

		return $url; // = http://example.com/path/directory

	}
}
