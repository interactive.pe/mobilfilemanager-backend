<?php

use Phalcon\Events\Event;
use Phalcon\Mvc\Micro;
use Phalcon\Mvc\Micro\MiddlewareInterface;
use Phalcon\Mvc\Router\Route;

/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

/**
 * FirewallMiddleware
 *
 * Checks the whitelist and allows clients or not
 */
class Cortafuegos implements MiddlewareInterface
{
	/**
	 * Before anything happens
	 *
	 * @param Event $event
	 * @param Micro $application
	 *
	 * @returns bool
	 */
	public function beforeHandleRoute(Event $event, Micro $app)
	{
		$request = $app->request;
		//$origen = $request->getHeader("ORIGIN") ? $request->getHeader("ORIGIN") : '*';
		$origen = '*';

		$app->response
			->setHeader("Access-Control-Allow-Origin", $origen )
			->setHeader(
				'Access-Control-Allow-Methods',
				'GET,PUT,POST,DELETE,OPTIONS'
			)
			->setHeader(
				'Access-Control-Allow-Headers',
				'Origin, X-Requested-With, Content-Range, ' .
					'Content-Disposition, Content-Type, Authorization'
			)
			->setHeader('Access-Control-Allow-Credentials', 'true');

		return true;
	}

	public function beforeExecuteRoute(Event $event, Micro $app){

		/*
		$ruta = $app->getRouter()->getMatchedRoute();
		$camino = explode( "/", $ruta->getPattern() );
		if ( $camino[1] == "api" ) {
			$r = $app->request;
			$appToken = $r->getHeader('HTTP_AUTHORIZATION');

			if ($r->isPost () || $r->isGet() ) {
				if ( $appToken != "Bearer " . $app->config->authentication->secret)
				{
					$app->response->setStatusCode( 401, "No autorizado" );
					$app->response->setContent( "" );
					$app->response->send();

					return false;
				}
			}
		}
		*/
		return true;
	}


	public function afterExecuteRoute(Event $event, Micro $app){
		return true;
	}

	public function afterHandleRoute(Event $event, Micro $app){
		return true;
	}


	/**
	 * Calls the middleware
	 *
	 * @param Micro $application
	 *
	 * @returns bool
	 */
	public function call(Micro $application)
	{
		return true;
	}
}
