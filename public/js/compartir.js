const W = window
const D = document

W.addEventListener("load", evt => {
	let archivos = D.querySelectorAll(".archivo")

	archivos.forEach(item => {
		item.querySelector("button.descargar").addEventListener("click", evt => {
			let boton = evt.currentTarget
			window.open(`/api/archivo/${boton.dataset.id}/descargar`)
		})
	})
})
//window.open(cfg.accion(`archivo/${id}/descargar`))
